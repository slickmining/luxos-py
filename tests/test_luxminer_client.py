import json
from unittest.mock import patch

import pytest

from luxos.luxminer.client import LuxMiner

from .data import (
    ADDPOOL,
    ASC_ZERO,
    ASCCOUNT,
    ATM,
    CHECK,
    COIN,
    CONFIG,
    DEVS,
    DISABLEPOOL,
    ENABLEPOOL,
    ESTATS,
    FANS,
    HEALTHCTRL,
    KILL,
    LCD,
    LIMITS,
    LOGON,
    POOLOPTS,
    POOLS,
    POWER,
    PROFILES,
    REMOVEPOOL,
    SESSION,
    STATS,
    SUMMARY,
    TEMPCTRL,
    TEMPS,
    TEMPSENSOR,
    VERSION,
)


def lux_ascii(function):
    def wrapper(data, endpoint):
        return json.dumps(function(data, endpoint)).encode("ascii")

    return wrapper


class TestLuxMiner:
    @patch.object(LuxMiner, "call_command")
    def test_display_methods(self, mock_call_command):
        client = LuxMiner("test")

        client.atm()
        assert mock_call_command.call_count == 1
        assert mock_call_command.call_args_list[0][0][0] == "atm"

        with pytest.raises(AttributeError):
            client.not_a_method()

    @lux_ascii
    def data(self, endpoint):
        d = {
            "atm": ATM,
            "asccount": ASCCOUNT,
            "check": CHECK,
            "coin": COIN,
            "config": CONFIG,
            "devs": DEVS,
            "edevs": DEVS,
            "estats": ESTATS,
            "fans": FANS,
            "healthctrl": HEALTHCTRL,
            "kill": KILL,
            "lcd": LCD,
            "limits": LIMITS,
            "logon": LOGON,
            "poolopts": POOLOPTS,
            "pools": POOLS,
            "power": POWER,
            "profiles": PROFILES,
            "session": SESSION,
            "stats": STATS,
            "summary": SUMMARY,
            "tempctrl": TEMPCTRL,
            "temps": TEMPS,
            "tempsensor": TEMPSENSOR,
            "version": VERSION,
            "asc": ASC_ZERO,
            "removepool": REMOVEPOOL,
            "addpool": ADDPOOL,
            "removepool": REMOVEPOOL,
            "disablepool": DISABLEPOOL,
            "enablepool": ENABLEPOOL,
            "tempsensor": TEMPSENSOR,
        }
        return d[endpoint]

    @pytest.mark.parametrize(
        "endpoint,args",
        [
            ("tempsensor", ()),
            ("session", ()),
            ("profiles", ()),
            ("power", ()),
            ("limits", ()),
            ("logon", ()),
            ("poolopts", ()),
            ("atm", ()),
            ("asccount", ()),
            ("config", ()),
            ("pools", ()),
            ("stats", ()),
            ("summary", ()),
            ("version", ()),
            ("lcd", ()),
            ("estats", ()),
            ("asc", (0,)),
            ("healthctrl", ()),
            ("removepool", (1,)),
            ("fans", ()),
            ("tempctrl", ()),
            ("devs", ()),
            ("temps", ()),
            (
                "addpool",
                ("stratum2+tcp://v2.us-east.stratum.braiins.com:3336", "user", "pass"),
            ),
            ("removepool", (2,)),
            ("disablepool", (1,)),
            ("enablepool", (1,)),
        ],
    )
    def test_endpoint(self, mocker, endpoint, args):
        data = self.data(endpoint)
        mocker.patch("socket.socket.connect")
        mocker.patch("socket.socket.send")
        recv = mocker.patch("socket.socket.recv")
        recv.side_effect = [data, None]
        client = LuxMiner("test")
        method = getattr(client, endpoint)
        response = method(*args)
        assert response == json.loads(data.decode("ascii"))
