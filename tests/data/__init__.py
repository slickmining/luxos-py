ATM = {
    "ATM": [
        {
            "Enabled": False,
            "MaxProfile": "default",
            "MinProfile": "",
            "PostRampMinutes": 15,
            "StartupMinutes": 15,
            "TempWindow": 5.0,
        }
    ],
    "STATUS": [
        {
            "Code": 339,
            "Description": "LUXminer 2023.7.7.220938-027e312",
            "Msg": "ATM configuration values",
            "STATUS": "S",
            "When": 1688824333,
        }
    ],
    "id": 1,
}

ASCCOUNT = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1683735143,
            "Code": 104,
            "Msg": "ASC count",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "ASCS": [{"Count": 3}],
    "id": 1,
}

LOGON = {
    "SESSION": [{"SessionID": "yJN2nlj1"}],
    "STATUS": [
        {
            "Code": 316,
            "Description": "LUXminer 0.1.0-15436f7140",
            "Msg": "Session created",
            "STATUS": "S",
            "When": 1667917312,
        }
    ],
    "id": 1,
}
CONFIG = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684153157,
            "Code": 33,
            "Msg": "BOSer config",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "CONFIG": [
        {
            "ASC Count": 3,
            "Device Code": "",
            "Hotplug": "None",
            "Log Interval": 5,
            "OS": "Braiins OS",
            "PGA Count": 0,
            "Pool Count": 2,
            "Strategy": "Failover",
        }
    ],
    "id": 1,
}

ASC_ZERO = {
    "ASC": [
        {
            "ASC": 0,
            "Accepted": 63,
            "Board": "S9Generic",
            "Connector": "J6",
            "ControllerIPVersionHex": "0x0",
            "ControllerIPVersionStr": "N/A",
            "Device Elapsed": 2312,
            "Device Hardware%": 0,
            "Device Rejected%": 0,
            "Diff1 Work": 2064384,
            "Difficulty Accepted": 2064384,
            "Difficulty Rejected": 0,
            "Enabled": "Y",
            "Hardware Error MHS 15m": 0,
            "Hardware Errors": 0,
            "ID": 0,
            "IsRamping": False,
            "IsUserShutdown": False,
            "Last Share Difficulty": 32768,
            "Last Share Pool": 1,
            "Last Share Time": 1672770053,
            "Last Valid Work": 1672770053,
            "MHS 15m": 4565123.138341468,
            "MHS 1m": 5034846.995524266,
            "MHS 30m": 4565123.138341468,
            "MHS 5m": 4527239.127367679,
            "MHS 5s": 4398046.511104,
            "MHS 60m": 4565123.138341468,
            "MHS av": 4540766.995326547,
            "Name": "",
            "Nominal MHS": 4668300.0,
            "Profile": "default",
            "Rejected": 0,
            "Status": "Alive",
            "Temperature": 76.0,
            "Total MH": 4540766.995326547,
            "Utility": 0,
        }
    ],
    "STATUS": [
        {
            "Code": 106,
            "Description": "cgminer 1.0.0",
            "Msg": "ASC0",
            "STATUS": "S",
            "When": 1672770089,
        }
    ],
    "id": 1,
}
POWER = {
    "POWER": [{"Watts": 1302}],
    "STATUS": [
        {
            "Code": 311,
            "Description": "LUXminer 0.1.0-15436f7140",
            "Msg": "Power usage",
            "STATUS": "S",
            "When": 1667917088,
        }
    ],
    "id": 1,
}
PROFILES = {
    "PROFILES": [
        {
            "Frequency": 650,
            "Hashrate": 9.6,
            "IsDynamic": True,
            "Profile Name": "default",
            "Voltage": 8.899999618530273,
            "Watts": 1305,
        },
        {
            "Frequency": 400,
            "Hashrate": 9.3,
            "IsDynamic": True,
            "Profile Name": "efficiency",
            "Voltage": 8.899999618530273,
            "Watts": 1022,
        },
        {
            "Frequency": 750,
            "Hashrate": 16.1,
            "IsDynamic": True,
            "Profile Name": "performance",
            "Voltage": 9.199999809265137,
            "Watts": 1535,
        },
    ],
    "STATUS": [
        {
            "Code": 323,
            "Description": "LUXminer 0.1.0-15436f7140",
            "Msg": "List profiles",
            "STATUS": "S",
            "When": 1667917088,
        }
    ],
    "id": 1,
}
SESSION = {
    "SESSION": [{"SessionID": ""}],
    "STATUS": [
        {
            "Code": 319,
            "Description": "LUXminer 0.1.0-15436f7140",
            "Msg": "Session information",
            "STATUS": "S",
            "When": 1667917088,
        }
    ],
    "id": 1,
}

TEMPSENSOR = {
    "STATUS": [
        {
            "Code": 347,
            "Description": "LUXminer 2023.12.13.205218-54b9ac9b",
            "Msg": "Temperature sensor",
            "STATUS": "S",
            "When": 1702591891,
        }
    ],
    "TEMPSENSOR": [{"BadAverageThreshold": 2, "MaxBadReadings": 10, "MinPerBoard": 1}],
    "id": 1,
}

POOLOPTS = {
    "POOLOPTS": [
        {
            "BackoffOnError": True,
            "MaxErrors": 5,
            "SmartSwitch": True,
            "SmartSwitchSecs": 60,
            "TimeoutSecs": 5,
        }
    ],
    "STATUS": [
        {
            "Code": 341,
            "Description": "LUXminer 2023.8.2.172357-c4963c8",
            "Msg": "Pool Options",
            "STATUS": "S",
            "When": 1691001482,
        }
    ],
    "id": 1,
}
KILL = {
    "SESSION": [{"SessionID": "VI2LCs8i"}],
    "STATUS": [
        {
            "Code": 318,
            "Description": "LUXminer 0.1.0-15436f7140",
            "Msg": "Kill session",
            "STATUS": "S",
            "When": 1667918597,
        }
    ],
    "id": 1,
}
LIMITS = {
    "LIMITS": [
        {
            "AtmMaxMinutes": 1440,
            "AtmMaxTemp": 10,
            "AtmMinMinutes": 1,
            "AtmMinTemp": 5,
            "AtmPostRampDefault": 15,
            "AtmStartupDefault": 15,
            "AtmTempWindowDefault": 5.0,
            "FanSpeedDefault": -1,
            "FanSpeedMax": 100,
            "FanSpeedMin": -1,
            "FrequencyDefault": 650,
            "FrequencyMax": 1175,
            "FrequencyMin": 100,
            "FrequencyStepMax": 1000,
            "FrequencyStepMin": 5,
            "HealthBadChipThresholdDefault": 0.2,
            "HealthBadChipThresholdMax": 1.0,
            "HealthBadChipThresholdMin": 0.1,
            "HealthNumReadingsDefault": 120,
            "HealthNumReadingsMax": 500,
            "HealthNumReadingsMin": 1,
            "MinFansDefault": 1,
            "MinFansMax": 4,
            "MinFansMin": 0,
            "PoolOptsMaxErrorsDefault": 5,
            "PoolOptsMaxErrorsMax": 999,
            "PoolOptsMaxErrorsMin": 0,
            "PoolOptsSmartSwitchSecsDefault": 60,
            "PoolOptsSmartSwitchSecsMax": 86400,
            "PoolOptsSmartSwitchSecsMin": 30,
            "PoolOptsTimeoutSecsDefault": 5,
            "PoolOptsTimeoutSecsMax": 30,
            "PoolOptsTimeoutSecsMin": 1,
            "TempSensorBadAverageThresholdDefault": 2,
            "TempSensorBadAverageThresholdMax": 5,
            "TempSensorBadAverageThresholdMin": 1,
            "TempSensorMaxBadReadingsDefault": 10,
            "TempSensorMaxBadReadingsMax": 20,
            "TempSensorMaxBadReadingsMin": 0,
            "TempSensorMinPerBoardDefault": 1,
            "TempSensorMinPerBoardMax": 4,
            "TempSensorMinPerBoardMin": 0,
            "TemperatureHot": 85,
            "TemperatureMax": 200,
            "TemperatureMin": 0,
            "TemperaturePanic": 90,
            "TemperatureTarget": 80,
            "UpdateTimeoutDefault": 60,
            "UpdateTimeoutMax": 10080,
            "UpdateTimeoutMin": 1,
            "VoltageDefault": 8.890000343322754,
            "VoltageMax": 9.5,
            "VoltageMin": 8,
            "VoltageStepMax": 10.0,
            "VoltageStepMin": 0.05,
        }
    ],
    "STATUS": [
        {
            "Code": 331,
            "Description": "LUXminer 2023.1.10-u0-b5809987d4",
            "Msg": "Config parameter limits",
            "STATUS": "S",
            "When": 1673874142,
        }
    ],
    "id": 1,
}
HEALTHCTRL = {
    "HEALTH": [{"NumReadings": 120, "BadChipHealthThreshold": 0.2}],
    "STATUS": [
        {
            "Code": 312,
            "Description": "LUXminer 0.1.0-15436f7140",
            "Msg": "Health config",
            "STATUS": "S",
            "When": 1667917088,
        }
    ],
    "id": 1,
}

POOLS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684335617,
            "Code": 7,
            "Msg": "2 Pool(s)",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "POOLS": [
        {
            "Accepted": 1937,
            "AsicBoost": True,
            "Bad Work": 0,
            "Best Share": 111132,
            "Current Block Height": 0,
            "Current Block Version": 536870916,
            "Diff1 Shares": 818721,
            "Difficulty Accepted": 207645124.0,
            "Difficulty Rejected": 580236.0,
            "Difficulty Stale": 0.0,
            "Discarded": 0,
            "Get Failures": 0,
            "Getworks": 363,
            "Has GBT": False,
            "Has Stratum": True,
            "Has Vmask": True,
            "Last Share Difficulty": 111132.0,
            "Last Share Time": 1684335612,
            "Long Poll": "N",
            "POOL": 0,
            "Pool Rejected%": 0.2786577004837451,
            "Pool Stale%": 0.0,
            "Priority": 0,
            "Proxy": "",
            "Proxy Type": "",
            "Quota": 1,
            "Rejected": 8,
            "Remote Failures": 0,
            "Stale": 0,
            "Status": "Alive",
            "Stratum Active": True,
            "Stratum Difficulty": 111132.0,
            "Stratum URL": "br.stratum.braiins.com:3333",
            "URL": "stratum+tcp://br.stratum.braiins.com:3333",
            "User": "florida622.moe",
            "Work Difficulty": 111132.0,
            "Works": 2465792000,
        },
        {
            "Accepted": 0,
            "AsicBoost": True,
            "Bad Work": 0,
            "Best Share": 0,
            "Current Block Height": 0,
            "Current Block Version": 0,
            "Diff1 Shares": 0,
            "Difficulty Accepted": 0.0,
            "Difficulty Rejected": 0.0,
            "Difficulty Stale": 0.0,
            "Discarded": 0,
            "Get Failures": 0,
            "Getworks": 0,
            "Has GBT": False,
            "Has Stratum": True,
            "Has Vmask": True,
            "Last Share Difficulty": 0.0,
            "Last Share Time": 0,
            "Long Poll": "N",
            "POOL": 1,
            "Pool Rejected%": 0.0,
            "Pool Stale%": 0.0,
            "Priority": 1,
            "Proxy": "",
            "Proxy Type": "",
            "Quota": 1,
            "Rejected": 0,
            "Remote Failures": 0,
            "Stale": 0,
            "Status": "Dead",
            "Stratum Active": False,
            "Stratum Difficulty": 0.0,
            "Stratum URL": "v2.br.stratum.braiins.com:3336",
            "URL": "stratum2+tcp://v2.br.stratum.braiins.com:3336",
            "User": "florida622.moe",
            "Work Difficulty": 0.0,
            "Works": 0,
        },
    ],
    "id": 1,
}

SUMMARY = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684335767,
            "Code": 11,
            "Msg": "Summary",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "SUMMARY": [
        {
            "Accepted": 1967,
            "Best Share": 111132,
            "Device Hardware%": 0.0,
            "Device Rejected%": 0.000938515503102967,
            "Difficulty Accepted": 210979084.0,
            "Difficulty Rejected": 580236.0,
            "Difficulty Stale": 0.0,
            "Discarded": 0,
            "Elapsed": 10061,
            "Found Blocks": 0,
            "Get Failures": 0,
            "Getworks": 370,
            "Hardware Errors": 0,
            "Last getwork": 1684335767,
            "Local Work": 2568355840,
            "MHS 15m": 95529355.91481896,
            "MHS 1m": 94734953.00194964,
            "MHS 24h": 10847623.919189552,
            "MHS 5m": 95555494.78071555,
            "MHS 5s": 93321229.40030123,
            "MHS av": 92537368.23150417,
            "Network Blocks": 0,
            "Pool Rejected%": 0.40506329113924056,
            "Pool Stale%": 0.0,
            "Rejected": 8,
            "Remote Failures": 0,
            "Stale": 0,
            "Total MH": 931077308373.0083,
            "Utility": 11.730444289832025,
            "Work Utility": 1301281.1933199605,
        }
    ],
    "id": 1,
}

STATS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684335793,
            "Code": 70,
            "Msg": "BOSer stats",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "STATS": [
        {
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Min": 0.0,
            "STATS": 0,
            "Wait": 0.0,
        },
        {
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Min": 0.0,
            "STATS": 1,
            "Wait": 0.0,
        },
        {
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Min": 0.0,
            "STATS": 2,
            "Wait": 0.0,
        },
        {
            "Bytes Recv": 0,
            "Bytes Sent": 0,
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Max Diff": 0.0,
            "Max Diff Count": 0,
            "Min": 0.0,
            "Min Diff": 0.0,
            "Min Diff Count": 0,
            "Net Bytes Recv": 0,
            "Net Bytes Sent": 0,
            "Pool Attempts": 0,
            "Pool Av": 0.0,
            "Pool Calls": 0,
            "Pool Max": 0.0,
            "Pool Min": 0.0,
            "Pool Wait": 0.0,
            "STATS": 3,
            "Times Recv": 0,
            "Times Sent": 0,
            "Wait": 0.0,
            "Work Can Roll": False,
            "Work Diff": 0.0,
            "Work Had Expire": False,
            "Work Had Roll Time": False,
            "Work Roll Time": 0,
        },
        {
            "Bytes Recv": 0,
            "Bytes Sent": 0,
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Max Diff": 0.0,
            "Max Diff Count": 0,
            "Min": 0.0,
            "Min Diff": 0.0,
            "Min Diff Count": 0,
            "Net Bytes Recv": 0,
            "Net Bytes Sent": 0,
            "Pool Attempts": 0,
            "Pool Av": 0.0,
            "Pool Calls": 0,
            "Pool Max": 0.0,
            "Pool Min": 0.0,
            "Pool Wait": 0.0,
            "STATS": 4,
            "Times Recv": 0,
            "Times Sent": 0,
            "Wait": 0.0,
            "Work Can Roll": False,
            "Work Diff": 0.0,
            "Work Had Expire": False,
            "Work Had Roll Time": False,
            "Work Roll Time": 0,
        },
    ],
    "id": 1,
}

VERSION = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684335821,
            "Code": 22,
            "Msg": "BOSer versions",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "VERSION": [{"API": "3.7", "BOSer": "boser-buildroot 0.1.0-0ce150e9"}],
    "id": 1,
}

ESTATS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684335841,
            "Code": 70,
            "Msg": "BOSer stats",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "STATS": [
        {
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Min": 0.0,
            "STATS": 0,
            "Wait": 0.0,
        },
        {
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Min": 0.0,
            "STATS": 1,
            "Wait": 0.0,
        },
        {
            "Calls": 0,
            "Elapsed": 0,
            "ID": "",
            "Max": 0.0,
            "Min": 0.0,
            "STATS": 2,
            "Wait": 0.0,
        },
    ],
    "id": 1,
}

CHECK = {
    "STATUS": [
        {
            "STATUS": "E",
            "When": 1684335871,
            "Code": 71,
            "Msg": "Missing check cmd",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "id": 1,
}

LCD = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684335892,
            "Code": 125,
            "Msg": "LCD",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "LCD": [
        {
            "Best Share": 0,
            "Current Pool": "",
            "Elapsed": 0,
            "Found Blocks": 0,
            "GHS 5m": 0.0,
            "GHS 5s": 0.0,
            "GHS av": 0.0,
            "Last Share Difficulty": 0.0,
            "Last Share Time": 0,
            "Last Valid Work": 0,
            "Temperature": 0.0,
            "User": "",
        }
    ],
    "id": 1,
}


FANS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358589,
            "Code": 202,
            "Msg": "4 Fan(s)",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "FANS": [
        {"FAN": 0, "ID": 0, "RPM": 3493, "Speed": 35},
        {"FAN": 1, "ID": 1, "RPM": 3493, "Speed": 35},
        {"FAN": 2, "ID": 2, "RPM": 3493, "Speed": 35},
        {"FAN": 3, "ID": 3, "RPM": 3493, "Speed": 35},
    ],
    "id": 1,
}

TEMPS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358589,
            "Code": 202,
            "Msg": "4 Fan(s)",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "FANS": [
        {"FAN": 0, "ID": 0, "RPM": 3493, "Speed": 35},
        {"FAN": 1, "ID": 1, "RPM": 3493, "Speed": 35},
        {"FAN": 2, "ID": 2, "RPM": 3493, "Speed": 35},
        {"FAN": 3, "ID": 3, "RPM": 3493, "Speed": 35},
    ],
    "id": 1,
}

COIN = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684361146,
            "Code": 78,
            "Msg": "BOSer coin",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "COIN": [
        {
            "Current Block Hash": "",
            "Current Block Time": 0.0,
            "Hash Method": "sha256",
            "LP": True,
            "Network Difficulty": 0.0,
        }
    ],
    "id": 1,
}


TEMPCTRL = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358639,
            "Code": 200,
            "Msg": "Temperature control",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "TEMPCTRL": [{"Dangerous": 90.0, "Hot": 80.0, "Mode": "Automatic", "Target": 60.0}],
    "id": 1,
}

TUNERSTATUS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358668,
            "Code": 203,
            "Msg": "Tuner Status",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "TUNERSTATUS": [
        {
            "ApproximateChainPowerConsumption": 2595,
            "ApproximateMinerPowerConsumption": 2595,
            "DynamicPowerScaling": "Disabled",
            "HashrateTargetTHS": 96.0,
            "PowerLimit": 0,
            "TunerChainStatus": [
                {
                    "ApproximatePowerConsumptionWatt": 2595,
                    "HashchainIndex": 0,
                    "Iteration": 0,
                    "LoadedProfileCreatedOn": 1684325723,
                    "PowerLimitWatt": 0,
                    "StageElapsed": 888,
                    "Status": "Stable",
                    "TunerRunning": True,
                    "TuningElapsed": 910,
                }
            ],
            "TunerMode": "hashrate_target",
        }
    ],
    "id": 1,
}

PAUSE = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358756,
            "Code": 204,
            "Msg": "Pause",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "PAUSE": [True],
    "id": 1,
}

RESUME = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358778,
            "Code": 205,
            "Msg": "Resume",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "RESUME": [True],
    "id": 1,
}

DEVS = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684358844,
            "Code": 9,
            "Msg": "3 ASC(s)",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "DEVS": [
        {
            "ASC": 0,
            "Accepted": 0,
            "Device Elapsed": 1086,
            "Device Hardware%": 0.0,
            "Device Rejected%": 0.0,
            "Diff1 Work": 29264,
            "Difficulty Accepted": 0.0,
            "Difficulty Rejected": 0.0,
            "Enabled": "Y",
            "Hardware Error MHS 15m": 0.0,
            "Hardware Errors": 0,
            "ID": 1,
            "Last Share Difficulty": 88688.0,
            "Last Share Pool": -1,
            "Last Share Time": 1684358835,
            "Last Valid Work": 1684358844,
            "MHS 15m": 30507928.38819819,
            "MHS 1m": 21575786.454763856,
            "MHS 5m": 28300878.39715383,
            "MHS 5s": 27482049.13055108,
            "MHS av": 32677538.22976102,
            "Name": "",
            "Nominal MHS": 27628429.973004,
            "Rejected": 0,
            "Status": "Alive",
            "Temperature": 0.0,
            "Total MH": 35505734426.82061,
            "Utility": 0.0,
        },
        {
            "ASC": 1,
            "Accepted": 0,
            "Device Elapsed": 1086,
            "Device Hardware%": 0.0,
            "Device Rejected%": 0.0,
            "Diff1 Work": 29585,
            "Difficulty Accepted": 0.0,
            "Difficulty Rejected": 0.0,
            "Enabled": "Y",
            "Hardware Error MHS 15m": 0.0,
            "Hardware Errors": 0,
            "ID": 2,
            "Last Share Difficulty": 88688.0,
            "Last Share Pool": -1,
            "Last Share Time": 1684358833,
            "Last Valid Work": 1684358844,
            "MHS 15m": 30905796.495183725,
            "MHS 1m": 30102258.964337964,
            "MHS 5m": 28598336.359637626,
            "MHS 5s": 32368820.203578606,
            "MHS av": 27527554.33118291,
            "Name": "",
            "Nominal MHS": 31548286.681128,
            "Rejected": 0,
            "Status": "Alive",
            "Temperature": 0.0,
            "Total MH": 29910031990.259712,
            "Utility": 0.0,
        },
        {
            "ASC": 2,
            "Accepted": 0,
            "Device Elapsed": 1086,
            "Device Hardware%": 0.0,
            "Device Rejected%": 0.0,
            "Diff1 Work": 28633,
            "Difficulty Accepted": 0.0,
            "Difficulty Rejected": 0.0,
            "Enabled": "Y",
            "Hardware Error MHS 15m": 0.0,
            "Hardware Errors": 0,
            "ID": 3,
            "Last Share Difficulty": 88688.0,
            "Last Share Pool": -1,
            "Last Share Time": 1684358830,
            "Last Valid Work": 1684358843,
            "MHS 15m": 29911223.805831373,
            "MHS 1m": 29787714.31607038,
            "MHS 5m": 27902920.574878998,
            "MHS 5s": 29800517.68684489,
            "MHS av": 24951258.90637547,
            "Name": "",
            "Nominal MHS": 31417123.973004,
            "Rejected": 0,
            "Status": "Alive",
            "Temperature": 0.0,
            "Total MH": 27110769875.222527,
            "Utility": 0.0,
        },
    ],
    "id": 1,
}

ADDPOOL = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684360032,
            "Code": 55,
            "Msg": "Added pool 2: 'stratum2+tcp://v2.us-east.stratum.braiins.com:3336'",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "id": 1,
}

REMOVEPOOL = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684360197,
            "Code": 68,
            "Msg": "Removed pool 2:'stratum2+tcp://v2.us-east.stratum.braiins.com:3336'",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "id": 1,
}

DISABLEPOOL = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684360251,
            "Code": 48,
            "Msg": "Disabling pool 1:'stratum2+tcp://v2.br.stratum.braiins.com:3336'",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "id": 1,
}

SWITCHPOOL = {
    "STATUS": [
        {
            "STATUS": "S",
            "When": 1684360277,
            "Code": 27,
            "Msg": "Switching to pool 1: 'stratum2+tcp://v2.br.stratum.braiins.com:3336'",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "id": 1,
}


ENABLEPOOL = {
    "STATUS": [
        {
            "STATUS": "I",
            "When": 1684360325,
            "Code": 49,
            "Msg": "Pool 1:'stratum+tcp://br.stratum.braiins.com:3333' already enabled",
            "Description": "BOSminer+ 0.2.0-ea64aec8e",
        }
    ],
    "id": 1,
}
