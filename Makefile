POETRY_BIN ?= ~/.local/bin/poetry

# ------------------------------------------
# dependency management
# ------------------------------------------

lock-no-update:
	$(POETRY_BIN) lock --no-update

lock-update:
	$(POETRY_BIN) lock

poetry-check-lock:
	$(POETRY_BIN) lock --check

setup: lock-update
	$(POETRY_BIN) install

setup-dev:
	$(POETRY_BIN) install --with=devtools --with=test

# ------------------------------------------
# git
# ------------------------------------------
setup-precommit: setup-dev
	rm -rf .git/hooks
	$(POETRY_BIN) run pre-commit install

hooks: setup-precommit

# ------------------------------------------
# newsfragments
# ------------------------------------------

towncrier_draft:
	$(POETRY_BIN) run towncrier --draft

towncrier_build:
	$(POETRY_BIN) run towncrier

# ------------------------------------------
# Test
# ------------------------------------------
test:
	$(POETRY_BIN) run pytest tests
# ------------------------------------------
# Lab
# ------------------------------------------
jupyter-lab:
	$(POETRY_BIN) run jupyter lab --notebook-dir=notebooks
