# luxos-py

Python client to retrieve data from any miner running LuxMiner Socket API


## Install

```
pip install luxminer
```

## Socket API Client Usage

```python
from luxos.luxminer.client import LuxMiner

host = 'moe.s5p8'
miner = LuxMiner(host)

print(miner.fans())
```

```json
{
   "STATUS": [
      {
         "STATUS": "S",
         "When": 1684941022,
         "Code": 202,
         "Msg": "4 Fan(s)",
         "Description": "BOSer boser-buildroot 0.1.0-0ce150e9"
      }
   ],
   "FANS": [
      {
         "FAN": 0,
         "ID": 0,
         "RPM": 3226,
         "Speed": 32
      },
      {
         "FAN": 1,
         "ID": 1,
         "RPM": 3315,
         "Speed": 32
      },
      {
         "FAN": 2,
         "ID": 2,
         "RPM": 3226,
         "Speed": 32
      },
      {
         "FAN": 3,
         "ID": 3,
         "RPM": 3255,
         "Speed": 32
      }
   ],
   "id": 1
}

```

### Implemented commands

All available commands are implemented. Check [LuxMiner API docs](https://docs.luxor.tech/firmware/api/available_commands).
