#!/usr/bin/python3
import json
import socket
from functools import partial
from typing import List


class LuxMiner(object):
    display_methods = {
        "atm": "Displays ATM configuration.",
        "asccount": "Returns the number of ASIC boards.",
        "coin": "Always zeros; exists only for compatibility.",
        "config": "Lists system configuration.",
        "devs": "Provides a detailed report of the ASIC boards.",
        "edevs": "Same as devs.",
        "estats": "Shows a variety of miner stats.",
        "fans": "Displays fan statistics.",
        "healthctrl": "Returns the current health check configuration.",
        "kill": "Kills the current active session and records this occurrence in the log file.",
        "lcd": "Always zeros; exists only for compatibility.",
        "limits": "Returns miner parameter limits.",
        "logon": "Creates a new session and returns its ID.",
        "poolopts": "Displays pool connection options.",
        "pools": "Lists all the pools, in order, and their statistics.",
        "power": "Returns the estimated power usage, in watts.",
        "profiles": "Lists the available profiles.",
        "session": "Returns the ID of the active session.",
        "stats": "Shows a variety of miner stats.",
        "summary": "Provides the summary of all miner statistics.",
        "tempctrl": "Displays temperature control.",
        "temps": "Shows temperature data.",
        "tempsensor": "Displays temperature sensor options.",
        "version": "Prints the API and application version.",
    }

    def __init__(self, miner_host):
        self.miner_host = miner_host
        for method_name, doc in self.display_methods.items():
            method = partial(self.call_command, method_name)
            method.__doc__ = doc
            setattr(self, method_name, method)

    def call_command(self, command, parameters: List = None):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.miner_host, 4028))
        command = {
            "command": command,
        }
        if parameters:
            command_parameters = {
                "parameters": ",".join([str(parameter) for parameter in parameters])
            }
            command.update(command_parameters)
        command = json.dumps(command) + "\n"
        command = bytes(command, "ascii")
        s.send(command)
        data = s.recv(2048)
        while True:
            page = s.recv(2048)
            if not page:
                break
            data += page
        s.close()
        response = json.loads(data.decode("ascii").rstrip("\x00"))
        del s
        return response

    def asc(self, n: int):
        return self.call_command("asc", [n])

    def switchpool(self, n: int):
        return self.call_command("switchpool", [n])

    def enablepool(self, n: int):
        return self.call_command("enablepool", [n])

    def disablepool(self, n: int):
        return self.call_command("disablepool", [n])

    def addpool(self, url: str, user: str, password: str):
        return self.call_command("addpool", [url, user, password])

    def removepool(self, n: int):
        return self.call_command("removepool", [n])
